package frc.robot;


import java.util.function.Supplier;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.commands.PathPlannerAuto;

import edu.wpi.first.util.sendable.Sendable;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;

/**
 * Responsible for selecting, compiling, and recompiling autos before the start of a match.
 */
public class AutoFactory {
    private final SendableChooser<Auto> autoSupplier;
    private Auto currentAuto;
    private Command compiledAuto;


    public AutoFactory(SendableChooser<Auto> autoSupplier) {
        this.autoSupplier = autoSupplier;
    }

    public Command getCompiledAuto() {
        return compiledAuto;
    }

    public boolean recompileNeeded() {
        return autoSupplier.getSelected() != currentAuto;
    }

    public void recompile() {
        SmartDashboard.putBoolean(Constants.DashboardConstants.AUTO_COMPILED_KEY, false);

        currentAuto = autoSupplier.getSelected();
        if (currentAuto == null) {
            currentAuto = Auto.NO_AUTO;
        }
        
        if (currentAuto.getAuto() != null) {
            compiledAuto = AutoBuilder.buildAuto(currentAuto.getAuto().getName());
            System.out.println("Current Auto: " + currentAuto.getAuto().getName());
        }

        SmartDashboard.putBoolean(Constants.DashboardConstants.AUTO_COMPILED_KEY, true);
    }


    public static enum Auto {
        // TODO - bjg- these auto names MUST match the pathplanner names - case sensitive.
        NO_AUTO(null),
        LEAVE_ZONE(new PathPlannerAuto("JustLeaveZone")), //replace w/ comma if you want to make more - ree
        SHOOT_AND_LEAVE(new PathPlannerAuto("ShootAndJustLeaveZone")),
        ANGLE_SHOOT_LEAVE(new PathPlannerAuto("ShootAndAngleLeave")),
        BLUE_LEFT_SHOOT_LEAVE(new PathPlannerAuto("BlueLeftSpeakerShootOut")),
        TEN_FEET_PATH(new PathPlannerAuto("10FeetAuto")),
        ONE_METER_AUTO(new PathPlannerAuto("OneMeterAuto")),
        MEH(new PathPlannerAuto("montysAuto")),
        JAMES_PATH(new PathPlannerAuto("james-auto")),
        SYLVIAS_AUTO(new PathPlannerAuto("sylvias-auto"));
        // AUTO_A387(new PathPlannerAuto("Auto-A387")),
        // AUTO_S54(new PathPlannerAuto("Auto-S54")),
        // AUTO_S45(new PathPlannerAuto("Auto-S45"));

        private final PathPlannerAuto auto;

        private Auto(PathPlannerAuto auto) {
            this.auto = auto;
        }

        public PathPlannerAuto getAuto(){
            return auto;
        }
    }
}