package frc.robot;

public class Constants {
    //public static final String robot = "SIM";
    public static final String robot = "Real";

    // Is tunable constant. Enables SysId routines in the autonomous selectable chooser.
    public static final boolean isTuningMode = false;

    public static class SwerveConstants {
        public static class Modules {
            public static class FrontLeft {
                public static int drive = 21;
                public static int turn = 22;
                public static int encoder = 23;
                public static double encoderOffset = 0.0;
            }
            public static class FrontRight {
                public static int drive = 11;
                public static int turn = 12;
                public static int encoder = 13;
                public static double encoderOffset = 0.0;
            }
            public static class BackLeft {
                public static int drive = 31;
                public static int turn = 32;
                public static int encoder = 33;
                public static double encoderOffset = 0.0;
            }
            public static class BackRight {
                public static int drive = 1;
                public static int turn = 2;
                public static int encoder = 3;
                public static double encoderOffset = 0.0;
            }
        }
    }

    // old WBL
    // public static class IntakeConstants {
    //     public static int gearRatio = 16;
    //     public static int intakeMotorID = 14;
    //     public static int pivotMotorID = 15;
    // }

    // public static class LaunchConstants {
    //     public static int launchMotorID = 16;
    //     public static int followMotorID = 17;
    // }

    public static final class pwmPortConstants {

        // public static final int kClimbMotors = 3;
        // public static final int kShootingTopMotors = 4;
      //  public static final int kClimbMotor = 4;
       // public static final int kIntakeMotors = 5;
        // public static final int kPuncher = 5;
       // public static final int kbottomShootMotors = 7;
       // public static final int kShooterMotors = 9;
        // TODO: these ports need to be configured to our robot!!! - reeha
    }

    // public static final class dioPortConstants {
    //     // public static final int intakeRollersCamera = 4;
    //     // public static final int kUpperAimLimitSwitch = 7;
    //     // public static final int kcargoLoadedSwitch = 8;
    // }

    // for camera configuration
    public static final class visionConstants {
        public static final int[] kCameraResolution = new int[] { 320, 240 };
        public static final int kCameraFPS = 15;
    }
    public static final class dioPortConstants {
        public static final int kEmitter = 0;   
        public static final int kReceiver = 7;

    }

    public static final class CANConstants {
      //  public static final int kLeftShooterMotor = 6;
      //  public static final int kRightShooterMotor = 7;

      //2  public static final int kClimbMotor = 5;

    }

    public static final class DashboardConstants {
        // AutoFactory - compile flag 
        public static final String AUTO_COMPILED_KEY = "Auto-Compiled-Key";
    }
}




