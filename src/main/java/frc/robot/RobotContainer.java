// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.Subsystem;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj2.command.sysid.SysIdRoutine;
import frc.robot.commands.DriveWithController;
import frc.robot.commands.IntakeBeamCmd;
import frc.robot.commands.IntakeRollersCmd;
import frc.robot.commands.ourCommands.ClimbDownCommand;
import frc.robot.commands.ourCommands.ClimbUpCommand;
import frc.robot.subsystems.CameraManager;
import frc.robot.subsystems.IntakeAndShootSS;
import frc.robot.subsystems.ClimbSubsystem.ClimbSS;
import frc.robot.subsystems.drive.Drive;
import frc.robot.subsystems.drive.GyroIO;
import frc.robot.subsystems.drive.GyroIONavX2;
import frc.robot.subsystems.drive.ModuleIOSim;
import frc.robot.subsystems.drive.ModuleIOSparkMax;

import com.fasterxml.jackson.databind.util.Named;
import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.auto.NamedCommands;
import com.pathplanner.lib.path.PathPlannerPath;

import frc.robot.AutoFactory;
import frc.robot.AutoFactory.Auto;


public class RobotContainer {
  // Subsystems
  private Drive drive;
  private IntakeAndShootSS intakeSubsystem;
  private ClimbSS climbSS;
  //private CameraManager m_cameraManager;
  private final CameraManager m_cameraManager = new CameraManager();

  //for the climb SS
  private final ClimbSS m_climbSS = new ClimbSS();

  // Controller
  private final CommandXboxController driveXbox = new CommandXboxController(1);
 // private final CommandXboxController manipulatorXbox = new CommandXboxController(1);
 XboxController shooterXbox = new XboxController(2);

  // Autonomous Routine Chooser - This may be changed if we move over to a solution such as PathPlanner that has it's own AutoBuilder. SmartDashboard for now :)
 // private final SendableChooser<Command> autoChooser = new SendableChooser<>();
  private final Command autoDefault = Commands.print("Default auto selected. No autonomous command configured.");

  SendableChooser<Auto> m_chooser = new SendableChooser<>();

  //member that declares AutoFactory
  private AutoFactory m_autoFactory;
  
  
  public RobotContainer() {
    switch (Constants.robot) {
      case "SIM":
        drive = 
          new Drive(
            new GyroIO() {},
            new ModuleIOSim(),
            new ModuleIOSim(),
            new ModuleIOSim(),
            new ModuleIOSim());

        intakeSubsystem = new IntakeAndShootSS(); // no simulation for intake subsystem
        break;
      case "Real":
        drive =
          new Drive(
            new GyroIONavX2() {},
            new ModuleIOSparkMax(0),
            new ModuleIOSparkMax(1),
            new ModuleIOSparkMax(2),
            new ModuleIOSparkMax(3));
        
        intakeSubsystem = new IntakeAndShootSS();
        break;
    }
    
    // Create missing drive subsystem if needed.
    if (drive == null) {
      drive =
        new Drive(
          new GyroIO() {},
          new ModuleIOSim(),
          new ModuleIOSim(),
          new ModuleIOSim(),
          new ModuleIOSim());
          
    }


    NamedCommands.registerCommand("testRegisterCmd", Commands.print("test command message"));
    
    NamedCommands.registerCommand("auto-shoot", getShootCommand());

    NamedCommands.registerCommand("intakeNamedCmd", intakeCommand());

   // NamedCommands.registerCommand("startClimb", startClimbCommand());

    

    // Add autonomous routines to the SendableChooser
    // autoChooser.setDefaultOption("Default Auto", autoDefault);
    // autoChooser.addOption("ShootMove", AutoBuilder.buildAuto("ShootMove"));
    // autoChooser.addOption("IntakeMove",  AutoBuilder.buildAuto("IntakeMove"));



  // A chooser for autonomous commands
    // m_chooser.addOption("3-note auto left side (blue), long", AutoBuilder.buildAuto("MediumAuto"));
    // m_chooser.addOption("2-note auto left side (blue), short", AutoBuilder.buildAuto("ShortAuto"));
    // m_chooser.addOption("2-note auto center (blue), short", AutoBuilder.buildAuto("CenterShortAuto"));
    // m_chooser.addOption("3-note auto center (blue), long", AutoBuilder.buildAuto("CenterLongAuto"));

    m_chooser.addOption("Only-Leave-Zone", Auto.LEAVE_ZONE);
    m_chooser.addOption("Shoot-And-Leave-Zone", Auto.SHOOT_AND_LEAVE);
    m_chooser.addOption("Angle-Shoot-Leave", Auto.ANGLE_SHOOT_LEAVE);
    m_chooser.addOption("Blue-Left-Shoot-Leave", Auto.BLUE_LEFT_SHOOT_LEAVE);
    m_chooser.addOption("10-feet-auto", Auto.TEN_FEET_PATH);
    m_chooser.addOption("1-meter-auto", Auto. ONE_METER_AUTO);
    m_chooser.addOption("montysTestPath", Auto.MEH);
    m_chooser.addOption("James's-Path", Auto.JAMES_PATH);
    m_chooser.addOption("sylvias-auto", Auto.SYLVIAS_AUTO);
    
    
    //YOU NEED TO DEPLOY BEFORE DRIVING AUTO!!!!
    //adding the chooser to the smartdashboard - ree
    SmartDashboard.putData(m_chooser);

     m_autoFactory = new AutoFactory(m_chooser);

    // TODO for competition - do we disable tuning mode?
    // if (Constants.isTuningMode) {
    //   m_chooser.addOption("Drive SysId (Dynamic Forward)", drive.sysIdDynamic(SysIdRoutine.Direction.kForward));
    //   m_chooser.addOption("Drive SysId (Dynamic Reverse)", drive.sysIdDynamic(SysIdRoutine.Direction.kReverse));
    //   m_chooser.addOption("Drive SysId (Quasistatic Forward)", drive.sysIdQuasistatic(SysIdRoutine.Direction.kForward));
    //   m_chooser.addOption("Drive SysId (Quasistatic Reverse)", drive.sysIdQuasistatic(SysIdRoutine.Direction.kReverse));
    // }

    // Put SendableChooser to SmartDashboard
    SmartDashboard.putData("Auto Chooser", m_chooser);

    //adding in camera - ree
    SmartDashboard.putData(m_cameraManager);


    configureBindings();
  }

 

  private void configureBindings() {
    // Joystick command factories
    // Function<Boolean, DriveWithController> driveWithControllerFactory =
    //     () ->
    //       new DriveWithController(
    //         drive,
    //         () -> xboxOne.getLeftX(),
    //         () -> xboxOne.getLeftY(),
    //         () -> xboxOne.getRightX(),
    //         () -> xboxOne.getRightY(),
    //         () -> xboxOne.a().getAsBoolean()
    //         );
    drive.setDefaultCommand(
      new DriveWithController(drive, () -> driveXbox.getLeftX(), () -> driveXbox.getLeftY(), () -> driveXbox.getRightX(), () -> driveXbox.getRightY(), () -> driveXbox.a().getAsBoolean())
    );
    // command for setting wheels to X configuration - to stop
     driveXbox.b().onTrue(Commands.runOnce(drive::stopWithX, drive));
    // System.out.println("Set default command");

     //will reset the navx to 0
    driveXbox.x().onTrue(Commands.runOnce(() -> drive.resetGyro())); 

    //b = resets the position of the wheels - ree
    //  driveXbox.b().onTrue(Commands.runOnce(() -> drive.setPose(new Pose2d(new Translation2d(), new Rotation2d(Units.degreesToRadians(180)))))); // from wbl - ree

    //right bumper = runs the climb motor down aka lifts us up - ree
    driveXbox.y().whileTrue(new ClimbDownCommand(m_climbSS)); 

    //left bumper = runs the climb motor up - ree
    driveXbox.a().whileTrue(new ClimbUpCommand(m_climbSS)); 

    /* incase i get super mad this is what i write when the driver wont listen to me - ree
    driveXbox.a().onTrue(getShootCommand()); /shoot speaker
    driveXbox.y().onTrue(new SequentialCommandGroup(new InstantCommand(intakeSubsystem::startShooterAmp), new WaitCommand(0.25), new InstantCommand(intakeSubsystem::startAmpBottom),
            new WaitCommand(1.5), new InstantCommand(intakeSubsystem::stopShooter))); //shoot amp but it waits
    
    */

    // Button 8 = Right Trigger, which is shooting the Speaker. on true, we start the shooter speaker, wait 2 seconds, then stop shooting
    new JoystickButton(shooterXbox, 8).onTrue(getShootCommand());

    // Button 7 = Left Trigger, which is shooting the Amp. on true, we start the shooter amp, wait 2 seconds, then stop shooting
    new JoystickButton(shooterXbox, 7)
        .onTrue(new SequentialCommandGroup(new InstantCommand(intakeSubsystem::startShooterAmp), new WaitCommand(0.25), new InstantCommand(intakeSubsystem::startAmpBottom),
            new WaitCommand(1.5), new InstantCommand(intakeSubsystem::stopShooter)));

    // Runs the intake motors while button 4/Y is pressed
    new JoystickButton(shooterXbox, 4).whileTrue(new IntakeRollersCmd(intakeSubsystem));
   // new JoystickButton(shooterXbox, 1).whileTrue(new IntakeRollersCmd(intakeSubsystem));

   //TODO: we need to change this button. b button. PLEASE CHANGE THIS SOON - ree
   new JoystickButton(shooterXbox,3).onTrue(intakeCommand());


    // X button - reverses the intake
    new JoystickButton(shooterXbox, 1).whileTrue(new InstantCommand(intakeSubsystem::reverseIntake));

    //a button - stops intake all together - ree
    new JoystickButton(shooterXbox, 2).onTrue(new InstantCommand(intakeSubsystem::stoppingIntake));
   
  
  }

  public Command getAutonomousCommand() {

   return m_autoFactory.getCompiledAuto();
   //return getShootCommand();
    }
    
    public void preCompileAuto() {
      if (m_autoFactory.recompileNeeded()) {
        m_autoFactory.recompile(); 
      }
    }

  public Command getShootCommand() {
     Command shootingSpeakerAuto = new SequentialCommandGroup(new InstantCommand(intakeSubsystem::startShooterSpeaker),
        new WaitCommand(0.75), new InstantCommand(intakeSubsystem::startSpeakerBottom),
        new WaitCommand(1.5), new InstantCommand(intakeSubsystem::stopShooter), new WaitCommand(0.2));

    return shootingSpeakerAuto;
  }

  public Command intakeCommand() {
      Command intakeCommand = new IntakeBeamCmd(intakeSubsystem);
      
      return intakeCommand;
  }

  // public Command startClimbCommand() {
  //   Command startClimb = new SequentialCommandGroup(new InstantCommand(climbSS::posClimb), new WaitCommand(8), new 
  //     InstantCommand(climbSS::stopClimb));

  //   return startClimb;
  // }

  /**
   * Create some code that runs a timed command to raise the climb hook
   * trigger this right away at match start (or at the end of autonomous)
   * 
   */
  // public void raiseHook() {
  //   Command raise = new ClimbUpCommand(m_climbSS).withTimeout(8);
  //   CommandScheduler.getInstance().schedule(raise);
  // }
  
}

