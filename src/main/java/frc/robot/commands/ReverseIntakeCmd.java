package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.IntakeAndShootSS;


public class ReverseIntakeCmd extends Command{
    
    private final IntakeAndShootSS m_IntakeAndShoot;

    public ReverseIntakeCmd(IntakeAndShootSS subsystem) {
        // System.out.println("intakeRollers (command): Constructed");
        m_IntakeAndShoot = subsystem;
        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(subsystem);

    }
    @Override
    public void initialize() {
        // put something here like a print system - reeha
    }

    @Override
    public void execute() {
        m_IntakeAndShoot.reverseIntake();
    }

    @Override
    public void end(boolean interrupted) {
      m_IntakeAndShoot.stoppingIntake();
    }

    @Override
    public boolean isFinished() {
        return false;
    }
    
}
