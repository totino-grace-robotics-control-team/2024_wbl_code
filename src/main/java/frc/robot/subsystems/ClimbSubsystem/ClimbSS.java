package frc.robot.subsystems.ClimbSubsystem;

import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkLowLevel.PeriodicFrame;
import com.revrobotics.CANSparkMax;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class ClimbSS extends SubsystemBase{
    
    //our constructors are going to be below - ree
   // private final CANSparkMax m_climb;

    // public ClimbSS() {
    //     super();
    //    // m_climb = initializeSparkMaxMotor(5);

    // }
 private CANSparkMax initializeSparkMaxMotor(int deviceID)
    {
        CANSparkMax mc = new CANSparkMax(deviceID, MotorType.kBrushless);

        mc.setCANTimeout(500);

        mc.setPeriodicFramePeriod(PeriodicFrame.kStatus2, 10);

        mc.setSmartCurrentLimit(20);

        mc.enableVoltageCompensation(12.0);

        mc.setCANTimeout(0);

      //  mc.burnFlash();

        mc.setIdleMode(IdleMode.kBrake);

        return mc;
    }


    //now come our methods
    public void posClimb() {
       double volts = 14;
   //     m_climb.setVoltage(volts); //TODO: subject to change, needs to be tested - ree
    }

    public void stopClimb() {
    //    m_climb.setVoltage(0);
    }

    public void negClimb() {
        double volts = -14;
    //    m_climb.setVoltage(volts);
    }
}
