package frc.robot.subsystems;

//imports below - reeha
import edu.wpi.first.wpilibj.motorcontrol.Spark;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkLowLevel.PeriodicFrame;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.dioPortConstants;
//import frc.robot.Constants.pwmPortConstants;

public class IntakeAndShootSS extends SubsystemBase {

    // private double m_intakeSpeed = -0.35; //used for the bottom intake wheels - ree
    // private double m_shooterSpeed = -0.45; //used for the top wheels - ree
    // private double m_shooterIntakeSpeed = .25;
    // private double m_conveyorSpeed = -0.25;
    // Bring the cargo in to the bucket
  //  private final Spark m_topShooter = new Spark(pwmPortConstants.kShootingTopMotors);
//    private final Spark m_intake = new Spark(pwmPortConstants.kIntakeMotors);
    
    // Shoots the NOTE!
    //private final Spark m_topShooter = new Spark(CANConstants.kLeftShooterMotor);
    //private final CANSparkMax m_topLeftShooter;
    //private final CANSparkMax m_topRightShooter;

   // private final Spark m_bottomShooter = new Spark(pwmPortConstants.kbottomShootMotors);
    //now is the beam break...
   // private final DigitalInput m_emitBeam = new DigitalInput(dioPortConstants.kEmitter);
    private final DigitalInput m_receiveBeam = new DigitalInput(dioPortConstants.kReceiver);
    

    public IntakeAndShootSS() {
        super();
        //m_topLeftShooter = initializeSparkMaxMotor(6);
        //m_topRightShooter = initializeSparkMaxMotor(7);

    }

    private CANSparkMax initializeSparkMaxMotor(int deviceID)
    {
        CANSparkMax mc = new CANSparkMax(deviceID, MotorType.kBrushless);

        mc.setCANTimeout(500);

        mc.setPeriodicFramePeriod(PeriodicFrame.kStatus2, 10);

        mc.setSmartCurrentLimit(20);

        mc.enableVoltageCompensation(12.0);

        mc.setCANTimeout(0);

       // mc.burnFlash();

        mc.setIdleMode(IdleMode.kBrake);

        return mc;
    }


    public void startShooterAmp() {
        double volts = 5;
        //m_topLeftShooter.setVoltage(volts);
        //m_topRightShooter.setVoltage(volts);

        // double bottomSpeed = 0.5;
        // m_bottomShooter.set(bottomSpeed);
       // System.out.println("current speed for the top shooter: " + m_topShooter + "current speed for the bottom shooter: " + m_bottomShooter);
    }
    
    public void startAmpBottom() {
        double bottomSpeed = 0.5;
    //    m_bottomShooter.set(bottomSpeed);
    }

    public void startSpeakerBottom() {
         double bottomSpeed = 0.85; //bjg
      //  m_bottomShooter.set(bottomSpeed);
    }

    public void startShooterSpeaker() {
        //double speakerMotorSpeed = 0.90; //bjg
        double volts = 12;
        // m_topLeftShooter.setVoltage(volts);
        // m_topRightShooter.setVoltage(volts);      //  m_bottomShooter.set(speakerMotorSpeed);
    }

    public void stopShooter() {
    // m_topLeftShooter.setVoltage(0);
    // m_topRightShooter.setVoltage(0);        
   // m_bottomShooter.set(0);
    }

    public void startingIntake() {
        double intakeMotorSpeed = 0.95;
      //  m_intake.set(intakeMotorSpeed);

        // TODO: Might need to change motor directions
        double bottomShooterspeed = 0.6;
      //  m_bottomShooter.set(bottomShooterspeed);
    }

    public void stoppingIntake() {
      //  m_intake.set(0);
     //   m_bottomShooter.set(0);
    }
    public boolean isNoteLoaded(){
        boolean currentState = !m_receiveBeam.get();
        //false is when the note/object isn't nearby/the beam isn't broken. if true, that means the beam is broken - ree
        return currentState; //to negate it, do !currentState
        
    }

    public void reverseIntake() {
       double volts = -6;
      // m_intake.setVoltage(volts);
     //  m_topLeftShooter.setVoltage(volts);
    //    m_topRightShooter.setVoltage(volts);

       double bottomIntakeSpeed = -0.6;
       // m_bottomShooter.set(bottomIntakeSpeed);
        //double speed = 0.5;
        //m_intake.set(speed);
    }

    }


